'use strict';

/**
 * Profile.js controller
 *
 * @description: A set of functions called "actions" for managing `Profile`.
 */

module.exports = {

  /**
   * Retrieve profile records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      let ret = await strapi.services.profile.search(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    } else {
      let ret = await strapi.services.profile.fetchAll(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    }
  },

  /**
   * Retrieve a profile record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    let ret = await strapi.services.profile.fetch(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Count profile records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.profile.count(ctx.query);
  },

  /**
   * Create a/an profile record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    let ret = await strapi.services.profile.add(ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Update a/an profile record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    let ret = await strapi.services.profile.edit(ctx.params, ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Destroy a/an profile record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    let ret = await strapi.services.profile.remove(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  nakheel_db_graphs: async (ctx) => {
    let flags = {
      "quantities": [1829, 1982, 1706, 1783, 1668],
      "rates": [0.3658, 0.3964, 0.3412, 0.3566, 0.3336]
    }
    let ret = await strapi.controllers.responseutils.responseWithFlags(flags);
    ctx.send(ret);
  },

  nakheel_db_map_regions: async (ctx) => {
    strapi.controllers.profile.nakheel_db_graphs(ctx);
  },

  nakheel_db_states: async (ctx) => {
    let flags = {
      "CIR": 0.2,
      "CIQ": 213,
      "CER": 0.3,
      "CER_arrow": "up",
      "IR": 1
    }
    let ret = await strapi.controllers.responseutils.responseWithFlags(flags);
    ret = JSON.parse(ret.body);
    ctx.send(ret);
  },

  nakheel_db_top_infected: async (ctx) => {
    let items = [{ "code": "CL-38", "region": "Dubai", "CIR": 0.35 }, { "code": "CL-82", "region": "Al-Ain", "CIR": 0.33 }, { "code": "CL-93", "region": "Dubai", "CIR": 0.30 }];
    let ret = await strapi.controllers.responseutils.responseWithItems(items);
    ret = JSON.parse(ret.body);
    ctx.send(ret);
  }
};
