'use strict';

const uuid = require('uuid');

/**
 * Farm.js controller
 *
 * @description: A set of functions called "actions" for managing `Farm`.
 */

module.exports = {

  /**
   * Retrieve farm records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      let ret = await strapi.services.farm.search(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    } else {
      let ret = await strapi.services.farm.fetchAll(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    }
  },

  /**
   * Retrieve a farm record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    let ret = await strapi.services.farm.fetch(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Count farm records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    let ret = await strapi.services.farm.count(ctx.query);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Create a/an farm record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    let ret = await strapi.services.farm.add(ctx.request.body);
    return strapi.controllers.responseutils.responseWithFlags({ uuid: ctx.request.body.uuid }, 'Farm has successuly added');
  },

  /**
   * Update a/an farm record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    let ret = await strapi.services.farm.edit(ctx.params, ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Destroy a/an farm record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    let ret = await strapi.services.farm.remove(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  nakheel_farm_db_graphs: async (ctx) => {
    let flags = {
      "quantities": [1829, 1982, 1706, 1783, 1668],
      "rates": [0.3658, 0.3964, 0.3412, 0.3566, 0.3336]
    };
    let ret = await strapi.controllers.responseutils.responseWithFlags(flags);
    ret = JSON.parse(ret.body);
    ctx.send(ret);
  },

  nakheel_farm_db_palms: async (ctx) => {
    ctx.query.farm_id = ctx.request.body.farm_id;
    let ret = await strapi.controllers.palm.find(ctx);
    ret = JSON.parse(ret.body);
    return ret;
  },

  nakheel_farm_db_states: async (ctx) => {
    let flags = {
      "CIR": 0.18,
      "CIQ": 52,
      "CER": 0.19,
      "CER_arrow": "up",
      "IR": 1
    };
    let ret = await strapi.controllers.responseutils.responseWithFlags(flags);
    ret = JSON.parse(ret.body);
    ctx.send(ret);
  },

  nakheel_list_farms: async (ctx) => {
    ctx.query.cluster_id = ctx.request.body.cluster_id;
    let ret = await strapi.controllers.farm.find(ctx);
    ret = JSON.parse(ret.body);
    return ret;
  },

  nakheel_farms_all: async (ctx) => {
    let ret = await strapi.controllers.farm.find(ctx);
    ret = JSON.parse(ret.body);
    return ret;
  },

  nakheel_load_farm: async (ctx) => {
    ctx.query.uuid = ctx.request.body.farm_id;
    let ret = await strapi.controllers.farm.find(ctx);
    ret = JSON.parse(ret.body);
    return ret;
  },

  nakheel_int_farms: async (ctx) => {
    return strapi.controllers.farm.nakheel_list_farms(ctx);    
  },

  nakheel_int_new_farm: async (ctx) => {
    let ret = await strapi.controllers.farm.create(ctx);
    return JSON.parse(ret.body);
  }
};
