'use strict';

/**
 * Responseutils.js controller
 *
 * @description: A set of functions called "actions" for managing `Responseutils`.
 */

var response = {
  "resp_code": 0,
  "resp_msg": { "en": "", "ar": "" },
  "flags": {},
  "items": []
};
var httpResponse = { 'statusCode': 200, 'body': response, "headers": { "Access-Control-Allow-Origin": "*" } };
const clear = async () => {
  response = {
    "resp_code": 0,
    "resp_msg": { "en": "", "ar": "" },
    "flags": {},
    "items": []
  }
  httpResponse = { 'statusCode': 200, 'body': response, "headers": { "Access-Control-Allow-Origin": "*" } };
}

const responseMessage = async (successMessage) => {
  clear();
  response.resp_code = 0;
  response.resp_msg.en = successMessage;
  httpResponse.body = JSON.stringify(response);
  return httpResponse;
}

const responseForError = async (errorMessage) => {
  clear();
  response.resp_code = 1;
  response.resp_msg.en = errorMessage;
  httpResponse.body = JSON.stringify(response);
  return httpResponse;
}

const responseWithItems = async (items, message) => {
  clear();
  response.resp_code = 0;
  if (message == undefined) { message = 'OK'; }
  response.resp_msg.en = message;
  response.items = items;
  httpResponse.body = JSON.stringify(response);
  return httpResponse;
}

const responseWithFlags = async (flags, message) => {
  clear();
  response.resp_code = 0;
  if (message == undefined) { message = 'OK'; }
  response.resp_msg.en = message;
  response.flags = flags;
  httpResponse.body = JSON.stringify(response);
  return httpResponse;
}

const responseWithFlagsAndItems = async (flags, items, message) => {
  clear();
  response.resp_code = 0;
  if (message == undefined) { message = 'OK'; }
  response.resp_msg.en = message;
  response.flags = flags;
  response.items = items;
  httpResponse.body = JSON.stringify(response);
  return httpResponse;
}

module.exports = {

  /**
   * Retrieve responseutils records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.responseutils.search(ctx.query);
    } else {
      return strapi.services.responseutils.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a responseutils record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.responseutils.fetch(ctx.params);
  },

  /**
   * Count responseutils records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.responseutils.count(ctx.query);
  },

  /**
   * Create a/an responseutils record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.responseutils.add(ctx.request.body);
  },

  /**
   * Update a/an responseutils record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.responseutils.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an responseutils record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.responseutils.remove(ctx.params);
  },
  response, httpResponse,
  responseForError, responseWithFlags, responseWithItems, responseWithFlagsAndItems, responseMessage
};
