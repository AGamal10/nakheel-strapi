'use strict';

/**
 * Device.js controller
 *
 * @description: A set of functions called "actions" for managing `Device`.
 */

module.exports = {

  /**
   * Retrieve device records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      let ret = await strapi.services.device.search(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    } else {
      let ret = await strapi.services.device.fetchAll(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    }
  },

  /**
   * Retrieve a device record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    let ret = await strapi.services.device.fetch(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Count device records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    let ret = await strapi.services.device.count(ctx.query);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Create a/an device record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    let ret = await strapi.services.device.add(ctx.request.body);
    ret = await strapi.controllers.responseutils.responseMessage('Add Item succeeded:{}');
    return JSON.parse(ret.body);
  },

  /**
   * Update a/an device record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    let ret = await strapi.services.device.edit(ctx.params, ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Destroy a/an device record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    let ret = await strapi.services.device.remove(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  ttn_device_graph: async (ctx) => {
    var rawValues = [];
    var timing = [];
    var end;

    let trouble = ['ec2-0004a30b0023a309', 'ec2-0004a30b0023a188',
      'ec2-0004a30b0023b101', 'ec2-0004a30b0023bfe6', 'ec2-0004a30b00214ef7'];

    function sliceArray(arr, length) {
      var len = arr.length;
      var subarray = [];
      if (len > length) {
        subarray = arr.slice(len - length + 1, len - 1)
        console.log('Sub Array: ' + JSON.stringify(subarray));
        return subarray;
      }
      else {
        console.log('Full Array: ' + JSON.stringify(arr));
        return arr;
      }
    }

    function decode(payload_raw) {
      let buff = new Buffer(payload_raw, 'base64');
      // console.log(buff.toString());
      let text = buff.toString('hex');
      var overallValue = parseInt(text, 16);
      // console.log('"' + payload_raw + '" converted from Base64 to ASCII is "' + text + '"');
      // console.log('Overal Value:' + overallValue);
      return overallValue;
    }

    const accTime = '2018-11-23T13:21:00.000000000Z';

    function badDevice(devid, timing) {
      return trouble.indexOf(devid) > -1 && timing > accTime;
    }

    function withinGap(curRecord, timing) {
      return Math.abs(Date.parse(timing) - curRecord.timing) < strapi.config.currentEnvironment.MINGAP;
    }

    function inBadDay(timing) {
      let date = new Date(timing);
      return date.getMonth() == 10 && (date.getDate() == 29 || date.getDate() == 30);
    }

    function loadRawValues(data, last, devid) {
      // var subdata = sliceArray(data, limit);
      // console.log("DATA HERE IS " + data);
      var subdata = data;
      rawValues = [];
      timing = [];
      // console.log(JSON.stringify(data));
      var curRecord = {
        number: -1,
        timing: 0
      };
      console.log(data);
      for (let val of subdata) {
        // console.log('VAL ' + val['payload_raw']);
        var yourNumber = decode(val['payload_raw']);

        // this is for handlinng devices that accumulate values
        let prevNum = yourNumber;
        if (withinGap(curRecord, val['time'])) continue;
        if (devid == 'ec2-0004a30b0023fca5' && inBadDay(val.time)) continue;
        if (badDevice(devid, val['time']) && curRecord.number != -1)
          yourNumber = yourNumber - curRecord.number;
        if (yourNumber < 0) yourNumber = 0;
        if (yourNumber != 170 && yourNumber != 187 && yourNumber <= 1000) {
          rawValues.push(yourNumber);
          timing.push(val['time']);
          curRecord.number = prevNum;
          curRecord.timing = Date.parse(val.time);
        }
      }
      rawValues = rawValues.slice(Math.max(0, rawValues.length - last));
      timing = timing.slice(Math.max(0, timing.length - last));
      var flags = { 'v': rawValues, 't': timing }
      var resp = strapi.controllers.responseutils.responseWithFlags(flags);
      return resp;
    }

    var devid = ctx.request.body.devid;
    var last = ctx.request.body.last;

    ctx.query.dev_id = devid;
    let data = await strapi.controllers.uplink.find(ctx);
    console.log(data);
    return loadRawValues(JSON.parse(data.body).items, last, devid);
  }
};
