'use strict';

const http = require('http');
const axios = require('axios');

/**
 * Lifecycle callbacks for the `Device` model.
 */

module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model) => {},

  // After saving a value.
  // Fired after an `insert` or `update` query.
  // afterSave: async (model, result) => {},

  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, results) => {},

  // Fired before a `fetch` operation.
  // beforeFetch: async (model) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, result) => {},

  // Before creating a value.
  // Fired before an `insert` query.
  // beforeCreate: async (model) => {

  //   http.get(`http://192.168.1.22:3000/add?devid=${model.uuid}`, function (res) { // need to make the ttn machine have a static IP
  //     // console.log('after get request');
  //   })
  //     .on('error', function (e) {
  //       console.error(e);
  //     })
  // },

  // After creating a value.
  // Fired after an `insert` query.
  // afterCreate: async (model, result) => {
  //   axios.post('http://localhost:1337/logs', {
  //     activity: 'ttn-int',
  //     time: new Date().getTime(),
  //     devEUI: model.uuid,
  //     devQR: model.qr_value
  //   }).then(function (res) {
  //     console.log('added to log', res);
  //   }).catch(function (err) {
  //     console.error(err);
  //   })
  // },

  // Before updating a value.
  // Fired before an `update` query.
  // beforeUpdate: async (model) => {},

  // After updating a value.
  // Fired after an `update` query.
  // afterUpdate: async (model, result) => {},

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, result) => {}
};
