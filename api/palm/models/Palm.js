'use strict';

const axios = require('axios');

/**
 * Lifecycle callbacks for the `Palm` model.
 */

module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model) => {},

  // After saving a value.
  // Fired after an `insert` or `update` query.
  // afterSave: async (model, result) => {},

  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, results) => {

  //   for (let item of results) {
  //     strapi.controllers.log.create({
  //       request: {
  //         body: {
  //           activity: 'palm_details_req',
  //           time: new Date().getTime(),
  //           devEUI: item.deviceuid,
  //           devQR: item.qr_value
  //         }
  //       }
  //     })
  //   }
  // },

  // Fired before a `fetch` operation.
  // beforeFetch: async (model) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, result) => {},

  // Before creating a value.
  // Fired before an `insert` query.
  // beforeCreate: async (model) => {
  //   let options = {
  //   };

  //   let params = {

  //   };
  //   const matchingType = model.qr_value !== "" ? 'qr' : 'deviceuid';
  //   if (matchingType == 'qr') {
  //     params.qr_value = model.qr_value;
  //   }
  //   else {
  //     params.deviceuid = model.deviceuid;
  //   }
  //   options.params = params;
  //   axios.get('http://localhost:1337/palms', options)
  //     .then(function (resp) {
  //       for (let item of resp.data) {
  //         if (item.id == model.id) continue;
  //         else {
  //           axios.put(`http://localhost:1337/palms/${item.id}`, {
  //             health_status: 'Unknown'
  //           })
  //             .then(resp => {
  //               console.log(`updated palm ${item.id}`);
  //             })
  //             .catch(err => {
  //               console.error(`error updating palm ${item.id}`);
  //             })
  //         }
  //       }
  //     }).catch(function (err) {
  //       console.error(err);
  //     })
  // },

  // After creating a value.
  // Fired after an `insert` query.
  // afterCreate: async (model, result) => {
  //   const matchingType = model.qr_value !== "" ? 'qr' : 'deviceuid';
  //   let params = model;
  //   if (matchingType == 'qr') {
  //     axios.get(`http://localhost:1337/devices?qr_value=${params.qr_value}`)
  //       .then(res => {
  //         console.log(res.data[0].uuid);
  //         axios.put(`http://localhost:1337/palms/${model.id}`, { deviceuid: res.data[0].uuid })
  //           .then(res => {
  //             console.log('updated current palm');
  //           })
  //           .catch(err => {
  //             console.log(err);
  //           });
  //       })
  //       .catch(err => {
  //         console.log(err);
  //       })
  //   }
  // },

  // Before updating a value.
  // Fired before an `update` query.
  // beforeUpdate: async (model) => {},

  // After updating a value.
  // Fired after an `update` query.
  // afterUpdate: async (model, result) => {},

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, result) => {}
};
