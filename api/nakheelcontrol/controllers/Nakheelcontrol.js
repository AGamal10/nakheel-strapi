'use strict';

/**
 * Nakheelcontrol.js controller
 *
 * @description: A set of functions called "actions" for managing `Nakheelcontrol`.
 */

module.exports = {

  /**
   * Retrieve nakheelcontrol records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {

    if (ctx.query._q) {

      let ret = await strapi.services.nakheelcontrol.search(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    } else {

      let ret = await strapi.services.nakheelcontrol.fetchAll(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);
    }
  },

  /**
   * Retrieve a nakheelcontrol record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    let ret = await strapi.services.nakheelcontrol.fetch(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Count nakheelcontrol records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    let ret = await strapi.services.nakheelcontrol.count(ctx.query);

    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Create a/an nakheelcontrol record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    let ret = await strapi.services.nakheelcontrol.add(ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Update a/an nakheelcontrol record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    let ret = await strapi.services.nakheelcontrol.edit(ctx.params, ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  /**
   * Destroy a/an nakheelcontrol record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    let ret = await strapi.services.nakheelcontrol.remove(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);
  },

  nakheel_int_controlData: async (ctx) => {
    ctx.query.devEUI = ctx.request.body.devEUI;
    ctx.query._sort = 'time:DESC';
    ctx.query._limit = 1;
    return strapi.controllers.nakheelcontrol.find(ctx);
  }
};
