'use strict';

/**
 * Uplink.js controller
 *
 * @description: A set of functions called "actions" for managing `Uplink`.
 */

module.exports = {

  /**
   * Retrieve uplink records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      let ret = await strapi.services.uplink.search(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);

    } else {
      let ret = await strapi.services.uplink.fetchAll(ctx.query);
      return strapi.controllers.responseutils.responseWithItems(ret);

    }
  },

  /**
   * Retrieve a uplink record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    let ret = await strapi.services.uplink.fetch(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);

  },

  /**
   * Count uplink records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    let ret = await strapi.services.uplink.count(ctx.query);
    return strapi.controllers.responseutils.responseWithItems(ret);

  },

  /**
   * Create a/an uplink record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    let ret = await strapi.services.uplink.add(ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);

  },

  /**
   * Update a/an uplink record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    let ret = await strapi.services.uplink.edit(ctx.params, ctx.request.body);
    return strapi.controllers.responseutils.responseWithItems(ret);

  },

  /**
   * Destroy a/an uplink record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    let ret = await strapi.services.uplink.remove(ctx.params);
    return strapi.controllers.responseutils.responseWithItems(ret);

  },

  nakheel_int_receivedUplink: async (ctx) => {
    strapi.controllers.uplink.create(ctx);
    let devCtx = ctx;
    devCtx.query.uuid = ctx.request.body.dev_id;
    if (devCtx.query.uuid.startsWith('ec2')) devCtx.query.uuid = devCtx.query.uuid.substring(4);
    let dev = await strapi.controllers.device.find(devCtx);

    let body = JSON.parse(dev.body);
    devCtx.query._id = body.items[0]._id;

    if (ctx.request.body.payload_raw === strapi.config.currentEnvironment.CONFIGPAYLOAD)
      devCtx.request.body.lastConfigTime = new Date(ctx.request.body.time).getTime();
    else devCtx.request.body.lastUplinkTime = new Date(ctx.request.body.time).getTime();

    return strapi.controllers.device.update(devCtx);
  },

  nakheel_int_periodicDeletion: async (ctx) => {
    const deleteRecord = async (item) => {
      let ctx = {
        query: {
          dev_id: item.dev_id,
          time: item.time
        }
      };
      let ret = await strapi.controllers.uplink.find(ctx);
      ctx.query._id = JSON.parse(ret.body).items[0]._id;
      return strapi.controllers.uplink.destroy(ctx);
    }
    function decode(payload_raw) {
      if (payload_raw == null) return 0;
      let buff = new Buffer(payload_raw, 'base64');

      let text = buff.toString('hex');
      var overallValue = parseInt(text, 16);

      return overallValue;
    }

    const accTime = '2018-11-23T13:21:00.000000000Z';
    let trouble = ['ec2-0004a30b0023a309', 'ec2-0004a30b0023a188',
      'ec2-0004a30b0023b101', 'ec2-0004a30b0023bfe6', 'ec2-0004a30b00214ef7'];

    function badDevice(devid, timing) {
      return trouble.indexOf(devid) > -1 && timing > accTime;
    }

    function withinGap(curRecord, timing) {
      return Math.abs(Date.parse(timing) - curRecord.timing) < strapi.config.currentEnvironment.MINGAP;
    }

    const getDeviceData = async (device) => {

      device = 'ec2-' + device;
      if (device.endsWith('\r\n')) device = device.slice(0, device.length - 2);

      let ctx = {
        query: {
          dev_id: device,
          _sort: 'time:DESC'
        }
      };
      let data = await strapi.controllers.uplink.find(ctx);
      let left = strapi.config.currentEnvironment.DELETETHRESHOLD;
      let curRecord = {
        number: -1,
        timing: 0
      };
      // console.log(data.Items.length);
      for (let item of data.Items) {
        if (left <= 0) {
          console.log(left);
          return strapi.controllers.responseutils.responseMessage('OK');
        }
        // console.log('VAL ' + val['payload_raw']);
        var yourNumber = decode(item['payload_raw']);

        // this is for handlinng devices that accumulate values
        let prevNum = yourNumber;
        // console.log(curRecord, item);
        if (withinGap(curRecord, item['time'])) {
          let ret = await deleteRecord(item);
          --left;
          // console.log(left)
          // console.log(left <= 0);
          if (left <= 0) {
            return strapi.controllers.responseutils.responseMessage('OK');

          }
        }
        else {
          if (badDevice(item.devid, item['time']) && curRecord.number != -1)
            yourNumber = yourNumber - curRecord.number;
          if (yourNumber < 0) yourNumber = 0;
          if (yourNumber != 170 && yourNumber != 187 && yourNumber <= 1000) {
            curRecord.number = prevNum;
            curRecord.timing = Date.parse(item.time);
          }
          else {
            let ret = await deleteRecord(item);
            --left;
            // console.log(left);
            // console.log(left <= 0)
            if (left <= 0) {
              console.log(left);
              return strapi.controllers.responseutils.responseMessage('OK');
            }
          }
        }
        // else deleteRecord(item);
      }
      // console.log("RAWVALUES = " + rawValues);
      // callback(null);

    }

    let device = ctx.request.body.dev_id;
    getDeviceData(device);
  }
};
