'use strict';

/**
 * Cron config that gives you an opportunity
 * to run scheduled jobs.
 *
 * The cron format consists of:
 * [MINUTE] [HOUR] [DAY OF MONTH] [MONTH OF YEAR] [DAY OF WEEK] [YEAR (optional)]
 */

const axios = require('axios');

const synchronize = async (data, find, create, update) => {
  for (let item of data) {
    let ret = await find({
      query: {
        uuid: item.uuid
      }
    });
    ret = JSON.parse(ret.body);
    if (ret.items.length === 0) {

      create({
        request: {
          body: item
        }
      })
    }
    else {
      update({
        params: {
          id: ret.items[0].id
        },
        request: {
          body: item
        }
      })
    }
  }
};

module.exports = {

  /**
   * Simple example.
   * Every monday at 1am.
   * '0 1 * * 1'
   */

  '*/5 * * * *': async () => {
    console.log('CRON');
    if (strapi.config.environments.development.FARM_ID !== 'ENTER FARM ID HERE') {
      axios.post(strapi.config.environments.development.API_SYNC_FARM, {
        'farm_id': strapi.config.environments.development.FARM_ID
      })
        .then(async (resp) => {
          synchronize(resp.data.items, strapi.controllers.farm.find, strapi.controllers.farm.create, strapi.controllers.farm.update);
        })
        .catch(err => {
          console.log(err);
        });

      axios.post(strapi.config.environments.development.API_SYNC_DEVICES_PALMS, {
        'farm_id': strapi.config.environments.development.FARM_ID
      })
        .then(async (resp) => {
          if (resp.data) {
            synchronize(resp.data.devices, strapi.controllers.device.find, strapi.controllers.device.create, strapi.controllers.device.update);
            synchronize(resp.data.palms, strapi.controllers.palm.find, strapi.controllers.palm.create, strapi.controllers.palm.update);
          }
        })
        .catch(err => {
          console.log(err);
        })
    }
    axios.get(strapi.config.environments.development.API_SYNC_CLUSTERS)
      .then(async (resp) => {
        resp = JSON.parse(resp.data.body);
        synchronize(resp.items, strapi.controllers.cluster.find, strapi.controllers.cluster.create, strapi.controllers.cluster.update);
      })
      .catch(err => {
        console.log(err);
      })

    axios.post(strapi.config.environments.development.API_SYNC_REGIONS)
      .then(async (resp) => {
        resp = resp.data;
        synchronize(resp.items, strapi.controllers.region.find, strapi.controllers.region.create, strapi.controllers.region.update);
      })
      .catch(err => {
        console.log(err);
      })
  }
};
